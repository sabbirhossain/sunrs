import * as React from 'react';
import { StyleSheet } from 'react-native';
import ModerateScreenInfo from '../components/moderateScreenInfo';
import { Text, View } from '../components/Themed';


export default function ModerateScreen() {
  return (
    <View style={styles.container}>
      
      <View lightColor="#eee" darkColor="rgba(255,255,200,0.1)" />
      <ModerateScreenInfo val="Two" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  
});
