import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { useState, useEffect } from 'react';
import { Icon } from 'react-native-elements';
import { ScrollView, StyleSheet, TouchableOpacity, Button, RefreshControl,SafeAreaView, ImageBackground } from 'react-native';
import * as Location from 'expo-location';
import Colors from '../constants/Colors';
import { MonoText } from './StyledText';
import { Text, View } from './Themed';
import { getSunrise, getSunset } from 'sunrise-sunset-js';
import { Ionicons } from '@expo/vector-icons';
import  bak  from '../assets/images/bak.jpg';




const wait = timeout => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};



export default function EditScreenInfo({ suns }: { suns: string }) {
   
   const [location, setLocation] = useState(null);
	  const [errorMsg, setErrorMsg] = useState(null);
	  const [responseJson, setResponseJson] = useState(null);
	  const [errorMsgt, setErrorMsgt] = useState(null);
    const [refreshing, setRefreshing] = React.useState(null);

    let text = 'Waiting..';
	  let su = "Waiting..";
	  let tempe = "Waiting for temperature ... ";
	  let fl = "";
    async function getWeather() {
      console.log("Inside getWeather");
      setRefreshing(true);
      let { status } = await Location.requestPermissionsAsync();
          if (status !== 'granted') {
          setErrorMsg('Permission to access location was denied');
          return;
          }
    
          
    
          let location = await Location.getCurrentPositionAsync({});
          console.log(location);
           
           
          setLocation(location);
          
          if(location)
          {
              let response = await fetch("https://api.openweathermap.org/data/2.5/weather?lat="+location.coords.latitude+"&lon="+location.coords.longitude+"&appid=54b17d375dcfc125e1623abba73ea704&units=metric");
            
            
                  let responseJson = await response.json();
            if (response.status !== 200) {
                      setErrorMsgt(responseJson.message);
                      
                  }
            else{
                
                setResponseJson(responseJson);
                console.log(responseJson);
            }
          
          }
          wait(5000).then(() => setRefreshing(false));
    };
    
	  
	  if(refreshing){
      text = 'Refreshing..';
    }
    
	  useEffect(() => {
		getWeather();
	  }, []);

	  
	  var t = new Date();
	  if (errorMsg) {
		text = errorMsg;
	  } else if (location) {
		if(suns == "One")
		{
		   su = getSunrise(location.coords.latitude,location.coords.longitude);
		   
		   console.log(t.getDate()+1);
		   if(su.getTime()<t.getTime()){
		     t.setDate(t.getDate()+1);
		     su = getSunrise(location.coords.latitude,location.coords.longitude, new Date(t.toString()));
		   }
			 
		   
		}
		else
		{
		   
           su = getSunset(location.coords.latitude,location.coords.longitude);
            
		   if(su.getTime()<t.getTime()){
			   t.setDate(t.getDate()+1);
		      su = getSunset(location.coords.latitude,location.coords.longitude,new Date(t.toString())); 
          
		   }
		}		   
		text = su.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
		if (errorMsgt) {
			tempe = errorMsgt;
		  } else if (responseJson) {
        if(suns=="One"){
		      tempe = "Temperature\n "+responseJson.main.temp+"\u00b0C";
			   fl = "Feels Like\n "+responseJson.main.feels_like+"\u00b0C";
         console.log(tempe);
        }
        else{
          tempe = "Pressure\n"+responseJson.main.pressure+" hPa";
			    fl = "Wind Speed\n"+responseJson.wind.speed+" meter/sec";
        }
		  }
		
	  }
   
  return (
    <View>
      <ImageBackground source={bak} style={styles.image}>
      <SafeAreaView style={styles.getStartedContainer}>
      
      <ScrollView
        contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}

          />
        }
       >
        <Text
          style={styles.getStartedText}
          lightColor="rgba(100,40,50,0.8)"
          darkColor="rgba(100,100,200,0.8)">
		  {text}
        </Text>
        <View style={styles.setRow}>
          <Text
            style={styles.getTempText}
            lightColor="rgba(160,10,160,0.8)"
            darkColor="rgba(55,10,55,0.8)">
          {tempe}  
          </Text>
      
        
          <Text
            style={styles.getTempText}
            lightColor="rgba(60,0,60,0.8)"
            darkColor="rgba(20,20,55,0.8)">
            {fl} 
          </Text>
          </View>
          <TouchableOpacity style={styles.checkButtonContainer} onPress={getWeather}>
        
        <Ionicons name="refresh-circle-outline" size={41} color="gray" />
        </TouchableOpacity>
        <Text
          style={styles.getEndText}
          lightColor="rgba(0,100,0,0.8)"
          darkColor="rgba(220,220,141,0.8)">
          Jazak Allahu Khairan
        </Text>
      </ScrollView>
      
    </SafeAreaView>
    </ImageBackground>
    </View>
    
  );
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/get-started/create-a-new-app/#opening-the-app-on-your-phonetablet'
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  checkButtonContainer: {
    borderRadius: 8,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  developmentModeText: {
    marginBottom: 20,
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  setRow:{
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgba(52, 52, 52, 0.0)',
    height: '10%',
    lineHeight: 30,
    
  },
  scrollView: {
    flex: 1,
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 40,
    marginTop: 110,
    justifyContent: 'center',
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 25,
    lineHeight: 30,
	fontWeight: 'bold',
    textAlign: 'center',
	
  },
  getEndText: {
    fontSize: 25,
    lineHeight: 30,
	fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 70,
	
  },
  getTempText: {
    fontSize: 18,
    lineHeight: 30,
	fontWeight: 'bold',
    textAlign: 'center',
    width: '50%',
    marginTop: 40,
    
  },
  
  helpContainer: {
    marginTop: 15,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: 'center',
  },
 
});
