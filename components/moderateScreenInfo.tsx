import * as WebBrowser from 'expo-web-browser';
import React from 'react';

import { useState, useEffect } from 'react';
import { Icon } from 'react-native-elements';
import { ScrollView, StyleSheet, TouchableOpacity, Button, RefreshControl,SafeAreaView, ImageBackground, Dimensions } from 'react-native';
import * as Location from 'expo-location';
import Colors from '../constants/Colors';
import { MonoText } from './StyledText';
import { Text, View } from './Themed';
import { getSunrise, getSunset } from 'sunrise-sunset-js';
import { Ionicons } from '@expo/vector-icons';
import  bak  from '../assets/images/scr0.jpg';

export default function ModerateScreenInfo({ val }: { val: string }) {
    const {API_URL} = process.env;
    const [data, setData] = useState(null);
    const [responseJson, setResponseJson] = useState(null);


    async function getData() {
      
      
      //let response = await fetch("https://www.google.com/search?q=oxygen+supply+near+me");
      
      //let responseJson = await response.json();
      //setResponseJson(responseJson);
      //console.log(responseJson);
      console.log("Moderate");
    
    };

    useEffect(() => {
      getData();
      }, []);
    console.log("out Data");
    return (
        <View>
          <ImageBackground source={bak} style={styles.image}>
            <SafeAreaView style={styles.getStartedContainer}>
            <Text
                style={styles.getStartedText}
                lightColor="rgba(100,40,50,0.8)"
                darkColor="rgba(100,100,200,0.8)">
               Short Velue
                
            </Text>
            
            </SafeAreaView>
          </ImageBackground>
        </View>   
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    image: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
      },
     
    getStartedText: {
        fontSize: 25,
        lineHeight: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 40,
        marginTop: 110,
        justifyContent: 'center',
    },
});